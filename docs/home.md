# api-demo

This is a demo for the api using Kestrel as a server.

## NuGet packages

To use Kesterl you need to add the following NuGet packages:

* ```Microsoft.AspNetCore.Server.Kestrel```
* ```Microsoft.AspNetCore.Mvc```
* ```Microsoft.AspNetCore```

## Https usage

Generate a developer certificate: ```dotnet dev-certs https```
and then trust the generated certificate ```dotnet dev-certs https --trust```

## Testing the new server

The server ist now available under: https://localhost:5001/ (port may be diffrent!)