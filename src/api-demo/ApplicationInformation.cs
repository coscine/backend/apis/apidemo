﻿using System;
using System.Collections.Generic;
using System.Net;

namespace coscine.api_demo
{
    public class ApplicationInformation
    {
        public string AppName { get; set; } = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
        public string AppType { get; set; } = "apis";
        public Version Version { get; set; } = ToSemanticVersion(System.Reflection.Assembly.GetEntryAssembly().GetName().Version);
        public string DomainName { get; set; } = System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties().DomainName;
        public string HostName { get; set; } = Dns.GetHostName();
        public string PathPrefix { get; set; } = $"coscine/api/{System.Reflection.Assembly.GetExecutingAssembly().GetName().Name}";
        public Tuple<int, int> PortRange = new Tuple<int, int>(6001, 6999);
        public int Port { get; set; } = 0;
        public string AppBasePath { get { return $"coscine/{AppType}/{AppName}"; } }
        public string TraefikBackendPath { get { return $"traefik/backends/{AppName}/servers/{HostName}"; } }
        public string TraefikFrontendPath { get { return $"traefik/frontends/{AppName}"; } }
        public Dictionary<string, string> AppValues
        {
            get
            {
                return new Dictionary<string, string>()
                {
                    { $"{AppBasePath}/port", $"{Port}" },
                    { $"{AppBasePath}/name", $"{AppName}" },
                    { $"{AppBasePath}/version", $"{Version}" }
                };
            }
        }
        public Dictionary<string, string> TraefikValues
        {
            get
            {
                return new Dictionary<string, string>()
                {
                    { $"{TraefikBackendPath}/url", $"http://{HostName}.{DomainName}:{Port}"},
                    { $"{TraefikBackendPath}/weight", $"{1}"},
                    { $"{TraefikFrontendPath}/backend", AppName},
                    { $"{TraefikFrontendPath}/routes/{AppName}/rule", $"Host:{HostName}.{DomainName};PathPrefix:/{PathPrefix}"}
                };
            }
        }

        private static Version ToSemanticVersion(Version version)
        {
            return new Version(version.Major, version.Minor, version.Build);
        }
    }
}
