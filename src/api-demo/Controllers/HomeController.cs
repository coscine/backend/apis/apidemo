﻿using Microsoft.AspNetCore.Mvc;

namespace coscine.api_demo.Controllers
{
    public class HomeController : Controller
    {
        [Route("[controller]")]
        public IActionResult Index()
        {
            return Ok("Hello World from a controller");
        }
        // For demonstration purpose
        public class Greeting
        {
            public string Username { get; set; }
        }

        //[Route("[controller]/greet/{username}")] would also work, but would take all commands
        [HttpGet("[controller]/greet/{username}")]
        public IActionResult Greet(string username)
        {
            var greeting = new Greeting { Username = username };
            return Ok(greeting);
        }
    }
}
