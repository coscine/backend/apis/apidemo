﻿using Coscine.Configuration;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System.IO;

namespace coscine.api_demo
{
    class Program
    {
        static void Main()
        {
            var configurator = new Configurator(new ConsulConfiguration());
            configurator.Register();
            var host = new WebHostBuilder()

            .ConfigureServices(services =>
            {
                services.AddSingleton(new Startup(configurator.ApplicationInformation));
            })
            .UseKestrel()
            .UseContentRoot(Directory.GetCurrentDirectory())
            .UseUrls($"http://[::]:{configurator.ApplicationInformation.Port}")
            .UseStartup<Startup>()
            .Build();

            host.Run();
        }        
    }
}
