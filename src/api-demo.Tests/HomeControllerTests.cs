﻿using NUnit.Framework;
using coscine.api_demo.Controllers;
using Microsoft.AspNetCore.Mvc;
using static coscine.api_demo.Controllers.HomeController;

namespace coscine.api_demo.Tests
{

    [TestFixture]
    public class HomeControllerTests
    {
        private readonly HomeController _homeController = new HomeController();

        [Test]
        public void GreetTest()
        {
            var name = "Laurin";

            var result = _homeController.Greet(name);
            Assert.IsInstanceOf<OkObjectResult>(result);
            var okResult = (OkObjectResult)result;

            Assert.IsInstanceOf<Greeting>(okResult.Value);
            var greeting = (Greeting)okResult.Value;
            Assert.IsTrue(greeting.Username == name);
        }

        [Test]
        public void IndexTest()
        {
            var answer = "Hello World from a controller";

            var result = _homeController.Index();
            Assert.IsInstanceOf<OkObjectResult>(result);
            var okResult = (OkObjectResult)result;

            Assert.IsInstanceOf<string>(okResult.Value);
            var answerFromController = (string)okResult.Value;
            Assert.IsTrue(answerFromController == answer);
        }
    }
}
