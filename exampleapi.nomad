job "exampleapi" {
  datacenters = ["dc1"]
  
  group "exampleapi" {
    task "exampleapi" {
      driver = "raw_exec"
    
      config {
        command = "C:/Programs/Example/api/api-demo.exe"
        args    = []
      }
    }
  }
}